package com.blackboard.platform.pie.service.impl;

import java.util.concurrent.ConcurrentHashMap;
import java.util.Collection;
import java.util.Map;

import com.blackboard.platform.pie.api.Pie;
import com.blackboard.platform.pie.dao.PieDao;
import com.blackboard.platform.pie.service.PieService;
import com.google.inject.Inject;

/**
 *
 */
public class InMemoryPieService implements PieService {
    private Map<String, Pie> map = new ConcurrentHashMap<>();

    public InMemoryPieService(){}

    @Override
    public Collection<Pie> getAll() {
        return map.values();
    }

    public void create(Pie pie) {
        map.put(pie.getUuid(), pie);
    }

    public void deleteAll() {
        map.clear();
    }
}
