package com.blackboard.platform.pie.service;

import com.blackboard.platform.pie.api.Pie;
import com.google.inject.Inject;

import java.util.Collection;

public interface PieService {

    Collection<Pie> getAll();

    void create(Pie pie);

    void deleteAll();
}
