package com.blackboard.platform.pie.resource.impl;

import com.blackboard.platform.pie.api.Pie;
import com.blackboard.platform.pie.resource.PieResource;
import com.blackboard.platform.pie.service.PieService;
import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;

import javax.ws.rs.core.Response;
import java.util.Collection;


public class PieResourceImpl implements PieResource {

    private final PieService service;

    @Inject
    public PieResourceImpl(PieService service){
        this.service = service;
    }

    public Collection<Pie>getAll(){
        return service.getAll();
    }

    public Response post(Pie pie) {
        service.create(pie);
        return Response.status(Response.Status.CREATED).entity(pie).build();
    }

    public Response delete() {
        service.deleteAll();
        return Response.ok().build();
    }
}
