package com.blackboard.platform.pie.resource;

import java.util.Collection;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.blackboard.platform.pie.api.Pie;
import com.codahale.metrics.annotation.Timed;


@Path("/pies")
@Produces(MediaType.APPLICATION_JSON)
public interface PieResource {

    @GET
    @Timed
    Collection<Pie> getAll();

    @POST
    @Timed
    Response post(Pie pie);

    @DELETE
    @Timed
    Response delete();
    
}
