package com.blackboard.platform.pie.health;

import com.codahale.metrics.health.HealthCheck;
import com.blackboard.platform.pie.AppConfiguration;

public class AppHealthCheck extends HealthCheck {

private final AppConfiguration configuration;

public AppHealthCheck(AppConfiguration configuration) {
        this.configuration = configuration;
        }


@Override
protected Result check() throws Exception {
        if (42 != 42) {
        return Result.unhealthy("App is unhealthy!");
        }
        return Result.healthy();
        }
        }
