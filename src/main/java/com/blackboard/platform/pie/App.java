package com.blackboard.platform.pie;

import com.blackboard.platform.pie.resource.PieResource;
import com.blackboard.platform.pie.health.AppHealthCheck;
import com.hubspot.dropwizard.guice.GuiceBundle;
import io.dropwizard.Application;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.auth.AuthValueFactoryProvider;
import io.dropwizard.auth.basic.BasicCredentialAuthFilter;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

/**
 * Platform Template
 *
 */
public class App extends Application<AppConfiguration> {

private GuiceBundle<AppConfiguration> guiceBundle;

public static void main(final String[]args)throws Exception {
    new App().run(args);
}

@Override
public String getName(){
    return "bb-pie";
}

@Override
public void initialize(final Bootstrap<AppConfiguration> bootstrap){

    guiceBundle=GuiceBundle.<AppConfiguration>newBuilder()
        .addModule(new AppModule())
        .setConfigClass(AppConfiguration.class)
        .build();

    bootstrap.addBundle(guiceBundle);
}


@Override
public void run(final AppConfiguration configuration, final Environment environment){

    // Setup the Authentication Providers
    // environment.jersey().register(new AuthDynamicFeature(
    // new BasicCredentialAuthFilter.Builder<PiePrincipalUserData>()
    // .setAuthenticator(new BasicPieAuthenticator(guiceBundle.getInjector().getInstance(PieUserLoaderService.class)))
    // .setAuthorizer(new PieAuthorizor(guiceBundle.getInjector().getInstance(PieRoleLookupService.class)))
    // .setRealm("Bb Platform Pie Realm")
    // .buildAuthFilter()));

    environment.jersey().register(RolesAllowedDynamicFeature.class);
    //If you want to use @Auth to inject a custom Principal type into your resource
    // environment.jersey().register(new AuthValueFactoryProvider.Binder<>(PiePrincipalUserData.class));

    // Register healthchecks here
    environment.healthChecks().register("PieCheck", new AppHealthCheck(configuration));

    // This can probably be removed altogether if we enable resource auto-detect
    environment.jersey().register(guiceBundle.getInjector().getInstance(PieResource.class));
    //environment.jersey().register(guiceBundle.getInjector().getInstance(OAuthTokenResource.class));
    //environment.jersey().register(guiceBundle.getInjector().getInstance(PieSecuredResource.class));
    //environment.jersey().register(guiceBundle.getInjector().getInstance(PieOpenResource.class));

    }
}