package com.blackboard.platform.pie;

import com.blackboard.platform.pie.resource.PieResource;
import com.blackboard.platform.pie.resource.impl.PieResourceImpl;
import com.blackboard.platform.pie.service.PieService;
import com.blackboard.platform.pie.service.impl.InMemoryPieService;
import com.google.inject.AbstractModule;


public class AppModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(PieService.class).to(InMemoryPieService.class);
        bind(PieResource.class).to(PieResourceImpl.class);
    }
}