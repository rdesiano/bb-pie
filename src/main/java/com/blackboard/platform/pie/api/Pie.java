package com.blackboard.platform.pie.api;

//import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Pie extends BaseDomain {
    //private UUID uuid;
    private String uuid;
    private String name;
    private String crust;
    private String description;
    
}
