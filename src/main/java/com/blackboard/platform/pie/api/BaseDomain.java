package com.blackboard.platform.pie.api;

import java.time.LocalDateTime;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BaseDomain {
    private String createdBy;
    private String modifiedBy;
    private LocalDateTime created;
    private LocalDateTime lastModified;

}
