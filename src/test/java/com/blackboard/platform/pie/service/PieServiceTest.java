package com.blackboard.platform.pie.service;

import static org.junit.Assert.assertTrue;
import com.blackboard.platform.pie.service.impl.InMemoryPieService;
import org.junit.Before;
import org.junit.Test;

/**
 *
 */
public class PieServiceTest {
    private PieService service;

    @Before
    public void setup() {
        service = new InMemoryPieService();
    }

    @Test
    public void someTest() {
        assertTrue(service.getAll().isEmpty());
    }
}
