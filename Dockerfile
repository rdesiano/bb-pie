FROM openjdk:8-jdk-slim
#ENV PORT 8080
#ENV CLASSPATH /opt/lib
EXPOSE 9000
EXPOSE 9001

COPY src/main/resources/application.yml /opt/conf/application.yml
ENV CONFIG_LOCATION /opt/conf/application.yml

COPY target/bb-pie-app*.jar /opt/app.jar
WORKDIR /opt
CMD ["sh", "-c", "java -jar /opt/app.jar server ${CONFIG_LOCATION}"]
